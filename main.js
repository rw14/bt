const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

var dssv = [];

// lấy thông tin từ localStorage
var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
// console.log("dssvJson", dssvJson);
if (dssvJson != null) {
  // array khi convert thanh json se mat fuction => cần map lại array

  dssv = JSON.parse(dssvJson);
  for (var index = 0; index < dssv.length; index++) {
    sv = dssv[index];
    dssv[index] = new SinhVien(
      sv.ma,
      sv.ten,
      sv.email,
      sv.password,
      sv.toan,
      sv.ly,
      sv.hoa
    );
  }

  renderDssv(dssv);
}
console.log(dssv);
function themSV() {
  var newSv = getInfor();
  // console.log(newSv);
  // kieerrm tra
  var isValid =
    validation.kiemTraRong(newSv.ma, "spanMaSV", "Mã không được để rỗng !") &&
    validation.kiemTraDoDai(
      newSv.ma,
      "spanMaSV",
      "Mã sinh viên không hợp lệ ! (Mã sinh viên phải gồm 4 kí tự) ",
      4,
      4
    );
  // ten
  isValid =
    isValid &
    validation.kiemTraTen(
      newSv.ten,
      "spanTenSV",
      "Tên không hợp lệ (Tên không được để trống và chứa số)"
    );
  // email
  isValid =
    isValid &
      validation.kiemTraRong(
        newSv.email,
        "spanEmailSV",
        "Email không được để trống !"
      ) &&
    validation.kiemTraMail(newSv.email, "spanEmailSV", "Email không hợp lệ");
  // matKhau
  isValid =
    isValid &
    validation.kiemTraRong(
      newSv.password,
      "spanMatKhau",
      "Mật khẩu không được để trống !"
    );
  // diemToan
  isValid =
    isValid &
    validation.kiemTraDiem(
      newSv.toan,
      "spanToan",
      "Điểm không hợp lệ ( Điểm >= 0)"
    );
  // ly
  isValid =
    isValid &
    validation.kiemTraDiem(
      newSv.ly,
      "spanLy",
      "Điểm không hợp lệ ( Điểm >= 0)"
    );
  // hoa
  isValid =
    isValid &
    validation.kiemTraDiem(
      newSv.hoa,
      "spanHoa",
      "Điểm không hợp lệ ( Điểm >= 0)"
    );

  if (isValid) {
    dssv.push(newSv);
  }
  // tao json
  var dssvJson = JSON.stringify(dssv);
  // lưu json vào localStorage
  localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
  // console.log("newSV", dssv);
  renderDssv(dssv);
}
function deleteSV(id) {
  // console.log(id);
  // var index = dssv.findIndex(function(sv){
  //   return sv.ma==id;
  // });
  var index = searchIndex(id, dssv);
  if (index != -1) {
    dssv.splice(index, 1);

    renderDssv(dssv);
  }
  var dssvJson = JSON.stringify(dssv);
  // lưu json vào localStorage
  localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
  // DSSV_LOCALSTORAGE
  // localStorage.setItem("DSSV_LOCALSTORAGE", JSON.getItem(dssv));
  // console.log(index);
}
function resetAll() {
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
  document.getElementById("spanMaSV").innerText = "";
  document.getElementById("spanTenSV").innerText = "";
  document.getElementById("spanEmailSV").innerText = "";
  document.getElementById("spanMatKhau").innerText = "";
  document.getElementById("spanToan").innerText = "";
  document.getElementById("spanLy").innerText = "";
  document.getElementById("spanHoa").innerText = "";
  document.getElementById("txtMaSV").disabled = false;
}

function editSV(id) {
  var index = searchIndex(id, dssv);
  if (index != -1) {
    var sv = dssv[index];
    showInfor(sv);
    // console.log(dssv[index]);
    document.getElementById("txtMaSV").disabled = true;
  }
}
// reset form

// update
function updateSV() {
  var editSV = getInfor();
  sv.ma = editSV.ma;
  // console.log(sv.ma);
  var index = searchIndex(sv.ma, dssv);
  // console.log(n);
  dssv[index] = editSV;

  // console.log(dssv);
  var dssvJson = JSON.stringify(dssv);
  // lưu json vào localStorage
  localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
  // console.log("newSV", dssv);
  renderDssv(dssv);
}
function searchName() {
  //   var s = document.getElementById("txtSearch").value;
  //  var tr
  //   var index = dssv.findIndex(function (dssv, index) {
  //     return dssv.ten === s;
  //   });
  //   console.log(index);
  //   var result = dssv[index];
  //   console.log(result);
  console.log("run");

  var input, filter, table, tr, td, txtValue, i;
  input = document.getElementById("txtSearch");
  filter = input.value.toUpperCase();
  table = document.getElementById("tbodySinhVien");
  // console.log(table);
  tr = table.getElementsByTagName("tr");
  for (var i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    // vi tri tim kiem index -> ten
    if (td) {
      txtValue = td.textContent || td.innerHTML;
      if (txtValue.toLocaleUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
