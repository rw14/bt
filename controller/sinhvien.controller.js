function getInfor() {
  const maSv = document.getElementById("txtMaSV").value;
  const tenSv = document.getElementById("txtTenSV").value;
  const email = document.getElementById("txtEmail").value;
  const password = document.getElementById("txtPass").value;
  const diemToan = document.getElementById("txtDiemToan").value;
  const diemLy = document.getElementById("txtDiemLy").value;
  const diemHoa = document.getElementById("txtDiemHoa").value;

  return new SinhVien(maSv, tenSv, email, password, diemToan, diemLy, diemHoa);
}
// document.getElementById("btnAdd").addEventListener("click", function () {});
function renderDssv(svArr) {
  // console.log("run");
  // console.log(dssv);
  // console.log("dssv", svArr);
  var contentHTML = "";
  // tao chuoi chua the tr ben duoi
  for (var i = 0; i < svArr.length; i++) {
    var sv = svArr[i];
    var trContent = `<tr>
    <td>${sv.ma}</td>
    <td>${sv.ten}</td>
    <td>${sv.email}</td>
    <td>${sv.tinhDTB()}</td>
    <td>
    <button class="btn btn-danger " onclick="deleteSV('${sv.ma}')" >Xóa</button>
    <button class="btn btn-warning " onclick="editSV('${sv.ma}')">Sửa</button>
    
    </td>
    </tr>`;

    contentHTML += trContent;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}
function searchIndex(id, dssv) {
  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];
    if (sv.ma == id) {
      // console.log(index);
      return index;
    }
  }
  // ko tifm thay
  return -1;
}
function showInfor(sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.password;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemHoa").value = sv.hoa;
}
