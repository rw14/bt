var validation = {
  kiemTraRong: function (value, idError, message) {
    if (value.length == 0) {
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraTen: function (value, idError, message) {
    if (value.match(/^[a-zA-Z\s]*$/) && value.length != 0) {
      document.getElementById(idError).innerText = "";

      return true;
    } else {
      document.getElementById(idError).innerText = message;

      return true;
    }
  },
  kiemTraDoDai: function (value, idError, message, min, max) {
    if (value.length < min || value.length > max) {
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraMail: function (value, idError, message) {
    // const re = ..

    // if (re.test(value)) {
    //   return true;
    // } else {

    //   return false;
    // }
    if (
      value.match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      )
    ) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = message;
      return false;
    }
  },
  kiemTraDiem: function (value, idError, message) {
    if (value.match(/^\d+(\.\d+)?$/)) {
      document.getElementById(idError).innerText = "";

      return true;
    } else document.getElementById(idError).innerText = message;

    return true;
  },
};
// mail format
// /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
